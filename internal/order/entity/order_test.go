package entity

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func Test_GivenAnEmptyID_WhenCretaANewOrder_ThenShouldReceiveAnError(t *testing.T) {
	order := Order{}
	assert.Error(t, order.IsValid(), "invalid ID")
}
func Test_GivenAnEmptyPrice_WhenCretaANewOrder_ThenShouldReceiveAnError(t *testing.T) {
	order := Order{ID: "123"}
	assert.Error(t, order.IsValid(), "invalid Price")
}
func Test_GivenAnEmptyTax_WhenCretaANewOrder_ThenShouldReceiveAnError(t *testing.T) {
	order := Order{ID: "1234", Price: 123}
	assert.Error(t, order.IsValid(), "invalid Tax")
}

func Test_GivenCorrectParams_WhenICallNewOrder_ThenIShouldReceiveCreateOrderWithAllParams(t *testing.T) {
	order := Order{
		ID:    "111",
		Price: 400,
		Tax:   10,
	}
	assert.Equal(t, "111", order.ID)
	assert.Equal(t, 400.0, order.Price)
	assert.Equal(t, 10.0, order.Tax)
	assert.Nil(t, order.IsValid())
}

func Test_GivenCorrectParams_WhenICallNewOrderFunc_ThenIShouldReceiveCreateOrderWithAllParams(t *testing.T) {
	order, err := NewOrder("123", 10.0, 2.0)
	assert.Nil(t, err)
	assert.Equal(t, "123", order.ID)
	assert.Equal(t, 10.0, order.Price)
	assert.Equal(t, 2.0, order.Tax)
}

func Test_GivenAPriceAndTax_WhenICallCalculateFinalPrice_ThenIShouldSetFinalPrice(t *testing.T) {
	order, err := NewOrder("123", 10.0, 2.0)
	assert.Nil(t, err)
	assert.Nil(t, order.CalculateFinalPrice())
	assert.Equal(t, 12.0, order.FinalPrice)
}
